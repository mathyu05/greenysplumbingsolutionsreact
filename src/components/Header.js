import PropTypes from 'prop-types'
import React from 'react'
import logo from '../images/logo.svg'

const Header = props => (
  <header id="header" style={props.timeout ? { display: 'none' } : {}}>
    <div className="logo">
      <img
        className="icon"
        src = { logo }
        alt = "Greeny's Plumbing Solutions Logo"
      />
    </div>
    <div className="content">
      <div className="inner">
        <h1>Greeny's Plumbing Solutions</h1>
        <p>
          All Cisterns Go!
        </p>
      </div>
    </div>
    <nav>
      <ul>
        <li>
          <button
            onClick={() => {
              props.onOpenArticle('about')
            }}
          >
            About
          </button>
        </li>
        <li>
          <button
            onClick={() => {
              props.onOpenArticle('contact')
            }}
          >
            Contact
          </button>
        </li>
        <li>
          <button
            onClick={() => {
              props.onOpenArticle('terms')
            }}
          >
            Terms
          </button>
        </li>
      </ul>
    </nav>
  </header>
)

Header.propTypes = {
  onOpenArticle: PropTypes.func,
  timeout: PropTypes.bool,
}

export default Header
