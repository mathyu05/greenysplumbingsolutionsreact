import PropTypes from 'prop-types'
import React from 'react'
import plumber from '../images/plumber.jpg'

class Main extends React.Component {
  render() {
    let close = (
      <div
        className="close"
        onClick={() => {
          this.props.onCloseArticle()
        }}
      ></div>
    )

    return (
      <div
        ref={this.props.setWrapperRef}
        id="main"
        style={this.props.timeout ? { display: 'flex' } : { display: 'none' }}
      >
        <article
          id="about"
          className={`${this.props.article === 'about' ? 'active' : ''} ${
            this.props.articleTimeout ? 'timeout' : ''
          }`}
          style={{ display: 'none' }}
        >
          <h2 className="major">About</h2>
          <span className="image main">
            <img src={plumber} alt="Plumber" />
          </span>
          <p>
            Established in 2017, Greeny's Plumbing Solutions is a small,
            { ' ' }
            family run business based in South East Queensland.
            { ' ' }
            We aim to offer a reliable,
            { ' ' }
            friendly and affordable service to all areas of Brisbane and surrounds.
            { ' ' }
            Greeny's Plumbing Solutions is the baby of Daniel Green,
            { ' ' }
            a plumber/gas fitter with many years experience in both commercial and residential plumbing works.
            { ' ' }
            Daniel has extensive experience in renovations, repairs and maintenance work.
            { ' ' }
            No job is too big or too small for Greeny's Plumbing Solutions so contact us today for a quote!
          </p>
          <p>
          Greeny's Plumbing Services offers a range of affordable,
          { ' ' }
          reliable and fully licenced plumbing, gas fitting, drainage and roofing services.
          </p>
          <p>
            ABN: 89 870 121 244
            <br />
            QBCC: 150 057 47
          </p>
          <p className="copyright">Built by Matthew Baldwin. Design by <a href="https://html5up.net" target="_blank" rel="noopener noreferrer">HTML5 UP</a>. Images by <a href="https://www.freepik.com/vectors" target="_blank" rel="noopener noreferrer">macrovector</a>.</p>
          {close}
        </article>

        <article
          id="contact"
          className={`${this.props.article === 'contact' ? 'active' : ''} ${
            this.props.articleTimeout ? 'timeout' : ''
          }`}
          style={{ display: 'none' }}
        >
          <h2 className="major">Contact</h2>
          <div className="field half first">
            <p className='label'>Call - 0439 301 957</p>
            <p className='label'>Email - admin@greenysplumbing.com.au</p>
          </div>
          <ul className="icons">
            <li>
              <a href="tel:0439301957" target="_blank" rel="noopener noreferrer" className="icon fa-phone">
                <span className="label">Phone</span>
              </a>
            </li>
            <li>
              <a href="mailto:admin@greenysplumbing.com.au" target="_blank" rel="noopener noreferrer" className="icon fa-envelope">
                <span className="label">Email</span>
              </a>
            </li>
            <li>
              <a href="https://www.facebook.com/greenysplumbingsolutions/" target="_blank" rel="noopener noreferrer" className="icon fa-facebook">
                <span className="label">Facebook</span>
              </a>
            </li>
          </ul>
          {close}
        </article>
      
        <article
          id="terms"
          className={`${this.props.article === 'terms' ? 'active' : ''} ${
            this.props.articleTimeout ? 'timeout' : ''
          }`}
          style={{ display: 'none' }}
        >
          <h2 className="major">Terms & Conditions</h2>
          
          <h3 className="major">Definitions</h3>
          <p className="clause">
            1.1 Greeny's Plumbing Solutions “GPS” shall mean the holder of an appropriate Licence with the authorised governing and licencing bodies and its successors and assigns.
          </p>
          <p className="clause">
            1.2 “Client” shall mean the Client or any person acting on behalf of and with the authority of the Client.
          </p>
          <p className="clause">
            1.3 “Guarantor” means that person (or persons), or entity who agrees herein to be liable for the debts of the Client on a principal debtor basis.
          </p>
          <p className="clause">
            1.4 “Goods” shall mean Goods supplied by GPS to the Client (and where the context so permits shall include any supply of Services as hereinafter defined).
          </p>
          <p className="clause">
            1.5 “Services” shall mean all services supplied by GPS to the Client and includes any advice or recommendations (and where the context so permits shall include any supply of Goods as defined).
          </p>
          <p className="clause">
            1.6 “Price” shall mean the cost of the Goods as agreed between GPS and the Client subject to clause 4 of this contract.
          </p>
          <p className="clause">
            1.7 “GPS” shall mean the Plumbing Company’s name “Greeny's Plumbing Solutions” which is referred to throughout, but not limited to Greeny's Plumbing Solutions' quotations, reports, invoices, receipts and these Terms and Conditions.
          </p>
          <p className="clause">
            1.8 “Site” means the place or places where the Customer’s work will be carried out.
          </p>
          <p className="clause">
            1.9 The failure of a party at any time to insist on performance of any obligation under these Terms is not a waiver of its right to insist on performance of that obligation or to claim damages unless that party acknowledges in writing that the failure is a waiver.
          </p>
          <p></p>

          <h3 className="major">Offer And Acceptance</h3>
          <p className="clause">
            2.1 Any instructions received by GPS from the Client for the supply of Goods and/or the Client’s acceptance of Goods supplied by GPS shall constitute acceptance of the terms and conditions contained herein.
          </p>
          <p className="clause">
            2.2 Where more than one Client has entered into this agreement, the Clients shall be jointly and severally liable for all payments of the Price.
          </p>
          <p className="clause">
            2.3 Upon acceptance of these terms and conditions by the Client the terms and conditions are irrevocable and can only be rescinded in accordance with these terms and conditions or with the written consent of GPS.
          </p>
          <p className="clause">
            2.4 None of GPS’s agents or representatives are authorised to make any representations, statements, conditions or agreements not expressed by the Plumbing Business of GPS in writing nor is GPS bound by any such unauthorised statements.
          </p>
          <p className="clause">
            2.5 Client to give GPS not less than fourteen (14) days prior written notice of any proposed change in the Client’s name and/or any other change in the Client’s details (including but not limited to, changes in the Client’s address or business practice).
          </p>
          <p></p>

          <h3 className="major">Goods</h3>
          <p className="clause">
            3.1 The Goods are as described on the invoices, quotation, work authorisation or any other work commencement forms as provided by GPS to the Client.
          </p>
          <p></p>

          <h3 className="major">Price And Payment</h3>
          <p className="clause">
            4.1 At GPS’s sole discretion the Price shall be either;
          </p>
          <p className="clause">
            (a) as indicated on invoices provided by GPS to the Client in respect of Goods supplied; or
          </p>
          <p className="clause">
            (b) Unless otherwise agreed in writing, all quotations given by GPS are valid for thirty (30) days only.
          </p>
          <p className="clause">
            4.2 Any variation from the plan of scheduled works or specifications will be charged for on the basis of GPS’s quotation and will be shown as variations on the invoice. Payment for all variations must be made in full at their time of completion.
          </p>
          <p className="clause">
            4.3 All works performed will be performed by GPS during business hours Monday to Friday 7.30am to 4pm unless otherwise stated in the Customer’s Job Order. Prices are quoted on the basis that works will be performed during business hours referred to in the clause. Work required by the Customer or otherwise necessitated outside of these hours, may increase the quoted price.
          </p>
          <p className="clause">
            4.4 At GPS’s sole discretion, payment for approved Clients shall be made by instalments in accordance with GPS’s delivery/payment schedule.
          </p>
          <p className="clause">
            4.5 At GPS’s sole discretion, for certain approved Clients, payment will be due on completion of works unless agreed and documented by both the customer and Greeny's Plumbing Solutions. This will be stated on the invoice, quotation or any other order forms. If no time is stated, then payment shall be on delivery of the Goods.
          </p>
          <p className="clause">
            4.6 Payment are to be made EFT (Electronic Funds Transfer) directly to Greeny's Plumbing Solutions bank account or by Visa, Master or debut cards, no surcharge will apply to these payments made (excluding American excluding payments). Other method as agreed to between the Client and GPS.
          </p>
          <p className="clause">
            4.7 In the event of a dispute, the complete undisputed portion of the Services (as determined by GPS acting reasonably) must be paid in accordance with the payment terms set out in these Terms.
          </p>
          <p className="clause">
            4.8 In the event that the Customer fails to make any payment by the due date, all monies owing to GPS will become immediately due and payable, and GPS may in its discretion:
          </p>
          <p className="clause">
            4.8.1 Cease Services, or withhold delivery of Goods, until all monies have been paid in full;
          </p>
          <p className="clause">
            4.8.2 Engage any debt collection service or person or initiate legal proceedings to recover all outstanding monies;
          </p>
          <p className="clause">
            4.8.3 Exercise all or any of its rights or powers of recovery or sale as provided for under these Terms.
          </p>
          <p className="clause">
            4.9 Interest on overdue amounts may be charged at a rate of 2.5% compounding per calendar month or part thereof and the Customer is liable for, and expressly undertakes to pay, all such interest
          </p>
          <p className="clause">
            5.0 All pricing is presented prior to GST and other taxes and duties which are applicable.
          </p>
          <p></p>

          <h3 className="major">Variations</h3>
          <p className="clause">
            6.1 Any requested variations, alterations or modifications to the Services ordered by the Customer must be put to Greeny's Plumbing Solutions in writing.
          </p>
          <p className="clause">
            6.2 Greeny's Plumbing Solutions in its absolute discretion may decide to accept or reject the variation, alteration or modification and will notify the Customer of its decision by any means.
          </p>
          <p className="clause">
            6.3 Where Greeny's Plumbing Solutions rejects any requested variations, alterations or modifications, and has undertaken, ordered or prepared Goods, the Customer will be required to accept the Goods and pay Greeny's Plumbing Solutions in accordance with the original quotation.
          </p>
          <p className="clause">
            6.4 Where any requested variation, alteration or modification is accepted, the original price quoted will be amended to reflect any consequential adjustment to the price of the Services or Goods.
          </p>
          <p></p>

          <h3 className="major">Delivery Of Goods/Services</h3>
          <p className="clause">
            7.1 Delivery of the Goods shall be made to the Client’s address. The Client shall make all arrangements necessary to take delivery of the Goods whenever they are tendered for delivery.
          </p>
          <p className="clause">
            7.2 GPS may deliver the Goods by separate instalments (in accordance with the agreed delivery schedule). Each separate instalment shall be invoiced and paid for in accordance with the provisions in this contract of sale.
          </p>
          <p className="clause">
            7.3 Delivery of the Goods to a third party nominated by the Client is deemed to be delivery to the Client for the purposes of this agreement.
          </p>
          <p className="clause">
            7.4 The Client shall take delivery of the Goods tendered notwithstanding that the quantity so delivered shall be either greater or less than the quantity purchased provided that: –
          </p>
          <p className="clause">
            (a) such discrepancy in quantity shall not exceed 5%, and
          </p>
          <p className="clause">
            (b) the Price shall be adjusted pro rata to the discrepancy.
          </p>
          <p className="clause">
            7.5 The failure of GPS to deliver shall not entitle either party to treat this contract as repudiated.
          </p>
          <p className="clause">
            7.6 GPS shall not be liable for any loss or damage, whatever due to failure by GPS to deliver the Goods (or any of them) promptly or at all.
          </p>
          <p className="clause">
            7.7 The Client shall ensure that GPS has clear and free access to the work site at all times to enable them to undertake the works. GPS shall not be liable for any loss or damage to the site (including, without limitation, damage to pathways, driveways and concreted or paved or grassed areas) unless due to the negligence of GPS.
          </p>
          <p></p>

          <h3 className="major">Delay In Services</h3>
          <p className="clause">
            8.1 Should the Services at the Site be held up for reasons beyond GPS control, including occasions GPS encounters Unfavourable Soil, then:
          </p>
          <p className="clause">
            8.2 GPS may recover the costs of such delay from the Customer;
          </p>
          <p className="clause">
            8.2.1 the Customer shall indemnify GPS from any increased costs, losses or expenses due to such delay; and to the extent permitted by law, GPS will not be liable for any loss or damage incurred by the Customer (or any other party).
          </p>
          <p></p>

          <h3 className="major">Risk</h3>
          <p className="clause">
            9.1 If GPS retains property in the Goods nonetheless, all risk for the Goods passes to the Client on delivery.
          </p>
          <p className="clause">
            9.2 If any of the Goods are damaged or destroyed prior to the property passing to the Client, GPS is entitled, without prejudice to any of its other rights or remedies under these Terms and Conditions of Trade (including the right to receive payment of the balance of the Price for the Goods), to receive all insurance proceeds payable for the Goods. This applies whether Price has become payable under the Contract. The production of these terms and conditions by GPS is enough evidence of GPS’s rights to receive the insurance proceeds without the need for any person dealing with GPS to make further enquiries.
          </p>
          <p></p>

          <h3 className="major">Client's Disclaimer</h3>
          <p className="clause">
            10.1 The Client hereby disclaims any right to rescind, or cancel the contract or to sue for damages or to claim restitution arising out of any misrepresentation made to him by any servant or agent of GPS. The Client acknowledges that he buys the Goods relying solely upon his own skill and judgement and that GPS shall not be bound by nor responsible for any term, condition, representation or warranty other than the warranty given by the Manufacturer which warranty shall be personal to the Client and shall not be transferable to any subsequent Client.
          </p>
          <p></p>

          <h3 className="major">Defects/Returns</h3>
          <p className="clause">
            11.1 The Client shall inspect the Goods on delivery and shall within one (1) week of delivery notify the GPS of any alleged defect, shortage in quantity, damage or failure to comply with the description or quote. The Client shall afford GPS an opportunity to inspect the Goods within a reasonable time following delivery if the Client believes the Goods are defective in any way. If the Client shall fail to comply with these provisions, the Goods shall be conclusively presumed to be in accordance with the terms and conditions and free from any defect or damage.
          </p>
          <p className="clause">
            11.2 For defective Goods, which GPS has agreed in writing that the Client is entitled to reject, GPS’s liability is limited to either (at GPS’s discretion) replacing the Goods or repairing the Goods provided that: –
          </p>
          <p className="clause">
            11.2.1 the Client has complied with the provisions of clause 8.1;
          </p>
          <p className="clause">
            11.2.2 GPS will not be liable for Goods which have not been stored or used in a proper manner;
          </p>
          <p className="clause">
            11.2.3 the Goods are returned in the condition in which they were delivered and with all packaging material, brochures and instruction material in as new condition as is reasonable possible in the circumstances.
          </p>
          <p></p>

          <h3 className="major">Warranty</h3>
          <p className="clause">
            12.1 For Goods not manufactured by GPS, the warranty shall be the current warranty provided by the manufacturer of the Goods. GPS shall be under no liability whatsoever, except for the express conditions as detailed and stipulated in the manufacturer’s warranty.
          </p>
          <p className="clause">
            12.2 Any warranty as to the Goods installed by GPS shall be limited to the written warranty provided by the manufacturer to the Customer on or before installation of the Good.
          </p>
          <p className="clause">
            12.3 GPS reserves the right to make null and void the warranty should the Goods or any part of the works be modified, altered, damaged or put to any undue stress other than in the way the Goods or works were designed to perform.
          </p>
          <p className="clause">
            12.4 GPS warrants that Goods supplied shall be of merchantable quality provided that the Goods are used for their intended purpose.  Where the Good is used contrary to any reasonable instructions provided by GPS the warranty is excluded
          </p>
          <p></p>

          <h3 className="major">The Commonwealth<br />Trade Practices Act And<br />Fair Trading Acts</h3>
          <p className="clause">
            13.1 Nothing in this agreement is intended to have the effect of contracting out of any applicable provisions of the Commonwealth Trade Practices Act, including the Federal Competition and consumer Act 2010 amendments, or the Fair Trading Acts in each of the States and Territories of Australia, except to the extent permitted by those Acts where applicable.
          </p>
          <p></p>

          <h3 className="major">Intellectual Property</h3>
          <p className="clause">
            14.1 All intellectual property created by or on behalf of GPS in relation to the Services supplied by GPS will be and remain the sole property of GPS. The Customer does not acquire any rights in GPS’s intellectual property under these Terms, other than the right to use such intellectual property within its business, and will not exploit, replicate, reverse-engineer or use for any other purpose GPS’s intellectual property or any materials or documents in which the intellectual property is recorded.
          </p>
          <p className="clause">
            14.2 The Customer warrants that GPS will not breach any third party’s intellectual property or other rights in consequence of supplying the Services in accordance with directions, instructions, drawings, designs or specifications provided by the Customer. The Customer indemnifies GPS against all liabilities, claims, losses, damages or costs (on a full indemnity basis and whether incurred by or awarded against GPS) that GPS may incur as a direct or indirect result of a breach of this warranty.
          </p>
          <p className="clause">
            14.3 The Customer acknowledges that all technical information, advice, know-how, drawings, designs and samples submitted to the Customer by GPS are confidential and the proprietary information of GPS. The Customer will keep all such information secret and confidential and will not disclose it or any part thereof to any person without the express written authority of GPS.
          </p>
          <p></p>

          <h3 className="major">Marketing Materials</h3>
          <p className="clause">
            The Customer grants to GPS the right to:
          </p>
          <p className="clause">
            15.1 Take photographs, film, videotape or other images of the works completed in or otherwise located at the Customer’s premises, and to use, reproduce, publish, edit, modify, dispose of or otherwise deal with those images; and
          </p>
          <p className="clause">
            15.2 Reproduce and publish the Customer’s name and trademarks and disclose the fact that GPS has provided Services to the Customer.
          </p>
          <p></p>

          <h3 className="major">Default And<br />Consequences Of Default</h3>
          <p className="clause">
            16.1 Interest on overdue invoices shall accrue from the date when payment becomes due daily until the date of payment at a rate of 2.5% compounding per calendar month and shall accrue at such a rate after as well as before any judgement.
          </p>
          <p className="clause">
            16.2 If the Client defaults in payment of any invoice when due, the Client shall indemnify GPS from and against all GPS’s costs and disbursements including on a solicitor and own client basis and in addition all GPS’s nominees’ costs of collection.
          </p>
          <p className="clause">
            16.3 Without prejudice to any other remedies GPS may have, if at any time the Client is in breach of any obligation (including those relating to payment), GPS may suspend or terminate the supply of Goods to the Client and any of its other obligations under the terms and conditions. GPS will not be liable to the Client for any loss or damage the Client suffers because GPS exercised its rights under this clause.
          </p>
          <p className="clause">
            16.4 If any account remains unpaid at the end of the second month after supply of the Goods or Services an immediate amount of the greater of $20.00 or 10.00% of the amount overdue shall be levied for administration fees which sum shall become immediately due and payable.
          </p>
          <p className="clause">
            16.5 In the event that: –
          </p>
          <p className="clause">
            16.5.1 any money payable to GPS becomes overdue, or in GPS’s opinion the Client will be unable to meet its payments as they fall due; or
          </p>
          <p className="clause">
            16.5.2 the Client becomes insolvent, convenes a meeting with its creditors or proposes or enters into an arrangement with creditors, or makes an assignment for the benefit of its creditors; or
          </p>
          <p className="clause">
            16.5.3 a receiver, manager, liquidator (provisional or otherwise) or similar person is appointed in respect of the Client or any asset of the Client, then without prejudice to GPS’s other remedies at law:
          </p>
          <p className="clause">
            16.5.4 GPS shall be entitled to cancel all or any part of any order of the Client which remains unperformed in addition to and without prejudice to any other remedies; and
          </p>
          <p className="clause">
            16.5.5 all amounts owing to GPS shall, whether or not due for payment, immediately become payable in addition to the interest payable under clause 12.1 hereof.
          </p>
          <p></p>

          <h3 className="major">Title</h3>
          <p className="clause">
            17.1 It is the intention of GPS and agreed by the Client that property of Goods shall not pass until:
          </p>
          <p className="clause">
            17.1.1 The Client has paid all amounts owing for the particular Goods, and
          </p>
          <p className="clause">
            17.1.2 The Client has met all other obligations due by the Client to GPS in respect of all contracts between GPS and the Client, and that where practicable the Goods shall be kept separate until GPS shall have received payment and all other obligations of the Client are met.
          </p>
          <p className="clause">
            17.2 Receipt by GPS of any form of payment other than cash shall not be deemed to be payment until that form of payment has been honoured, cleared or recognised and until then GPS’s ownership of rights in respect of the Goods shall continue.
          </p>
          <p className="clause">
            17.3 It is further agreed that: –
          </p>
          <p className="clause">
            17.3.1 Until such time as ownership of the Goods shall pass from GPS to the Client GPS may give notice in writing to the Client to return the Goods or any of them to GPS. Upon such notice the rights of the Client to obtain ownership or any other interest in the Goods shall cease.
          </p>
          <p className="clause">
            17.3.2 GPS shall have the right of stopping the Goods in transit whether or not delivery has been made; and
          </p>
          <p className="clause">
            17.3.3 If the Client fails to return the Goods to GPS, then an GPS agent may enter upon and into land and premises owned, occupied or used by the Client, or any premises as the invitee of the Client, where the Goods are situated and take possession of the Goods.
          </p>
          <p className="clause">
            17.3.4 The Client is only a bailee of the Goods and until such time as GPS has received payment in full for the Goods then the Client shall hold any proceeds from the sale or disposal of the Goods on trust for GPS.
          </p>
          <p className="clause">
            17.3.5 The Client shall not deal with the money of GPS in any way which may be adverse to GPS.
          </p>
          <p className="clause">
            17.3.6 The Client shall not charge the Goods in any way nor grant nor otherwise give any interest in the Goods while they remain the property of GPS.
          </p>
          <p className="clause">
            17.3.7 GPS may require payment of the Price or the balance of the Price due together with any other amounts due from the Client to GPS arising out of these terms and conditions, and GPS may take any lawful steps to require payment of the amounts due and the Price.
          </p>
          <p className="clause">
            17.3.8 GPS can issue proceedings to recover the Price of the Goods sold notwithstanding that ownership of the Goods may not have passed to the Client.
          </p>
          <p className="clause">
            17.3.9 Until such time that ownership in the Goods passes to the Client, if the Goods are so converted, the parties agree that GPS will be the owner of the end products.
          </p>
          <p></p>

          <h3 className="major">Goods</h3>
          <p className="clause">
            3.1 The Goods are as described on the invoices, quotation, work authorisation or any other work commencement forms as provided by GPS to the Client.
          </p>
          <p></p>

          <h3 className="major">Security And Charge</h3>
          <p className="clause">
            18.1 Despite anything to the contrary contained herein or any other rights which GPS may have howsoever: –
          </p>
          <p className="clause">
            (a) Where the Client and/or the Guarantor (if any) is the owner of land, realty or any other asset capable of being charged, both the Client and/or the Guarantor agree to mortgage and/or charge all of their joint and/or several interest in the said land, realty or any other asset to GPS or GPS’s nominee to secure all amounts and other monetary obligations payable under the terms and conditions. The Client and/or the Guarantor acknowledge and agree that GPS (or GPS’s nominee) shall be entitled to lodge where appropriate a caveat, which caveat shall be released once all payments and other monetary obligations payable hereunder have been met.
          </p>
          <p className="clause">
            (b) Should GPS elect to proceed in any manner in accordance with this clause and/or its sub-clauses, the Client and/or Guarantor shall indemnify GPS from and against all GPS’s costs and disbursements including legal costs on a solicitor and own client basis.
          </p>
          <p className="clause">
            (c) To give effect to the provisions of clause [18.1 (a) and (b)] inclusive hereof the Client and/or the Guarantor (if any) do hereby irrevocably nominate constitute and appoint GPS or GPS’s nominee as the Client’s and/or Guarantor’s true and lawful attorney to execute mortgages and charges (whether registerable or not) including such other terms and conditions as GPS and/or GPS’s nominee shall think fit in his/her/its/their absolute discretion against the joint and/or several interest of the Client and/or the Guarantor in any land, realty or asset in favour of GPS and in the Client’s and/or Guarantor’s name as may be necessary to secure the said Client’s and/or Guarantor’s obligations and indebtedness to GPS and further to do and perform all necessary and other acts including instituting any necessary legal proceedings, and further to execute all or any documents in GPS's absolute discretion which may be necessary or advantageous to give effect to the provisions of this clause.
          </p>
          <p></p>

          <h3 className="major">Cancellation</h3>
          <p className="clause">
            19.1 GPS may cancel these terms and conditions or cancel delivery of Goods at any time before the Goods are delivered by giving written notice. GPS shall not be liable for any loss or damage whatever arising from such cancellation. At GPS sole discretion the Client may cancel delivery of Goods. If the Client cancels delivery of Goods, the Client shall be liable for any costs incurred by GPS up to the time of cancellation.
          </p>
          <p></p>

          <h3 className="major">Privacy Act 1988</h3>
          <p className="clause">
            20.1 The Client and/or the Guarantor/s agree for GPS to obtain from a credit-reporting agency a credit report containing personal credit information about the Client and Guarantor/s in relation to credit provided by GPS.
          </p>
          <p className="clause">
            20.2 The Client and/or the Guarantor/s agree that GPS may exchange information about Client and Guarantor/s with those credit providers named in the Application for Credit account or named in a consumer credit report issued by a reporting agency for the following purposes: –
          </p>
          <p className="clause">
            (a) To assess an application by Client;
          </p>
          <p className="clause">
            (b) To notify other credit providers of a default by the Client;
          </p>
          <p className="clause">
            (c) To exchange information with other credit providers as to the status of this credit account, where the Client is in default with other credit providers; and
          </p>
          <p className="clause">
            (d) To assess the credit worthiness of Client and/or Guarantor/s.
          </p>
          <p className="clause">
            20.3 The Client consents to GPS being given a consumer credit report to collect overdue payment on commercial credit (Section 18K(1)(h) Privacy Act 1988).
          </p>
          <p className="clause">
            20.4 The Client agrees that Personal Data provided may be used and retained by GPS for the following purposes and for other purposes as shall be agreed between the Client and GPS or required by law from time to time: –
          </p>
          <p className="clause">
            20.4.1 Provision of Services & Goods;
          </p>
          <p className="clause">
            20.4.2 Marketing of Services and/or Goods by GPS, its agents or distributors in relation to the Services and Goods;
          </p>
          <p className="clause">
            20.4.3 Analysing, verifying and/or checking the Client’s credit, payment and/or status in relation to provision of Services/Goods;
          </p>
          <p className="clause">
            20.4.4 Processing of any payment instructions, direct debit facilities and/or credit facilities requested by Client; and
          </p>
          <p className="clause">
            20.4.5 Enabling the daily operation of Client’s account and/or the collection of amounts outstanding in the Client’s account in relation to the Services and Goods.
          </p>
          <p className="clause">
            20.4.6 GPS may give, information about the Client to a credit reporting agency for the following purposes:
          </p>
          <p className="clause">
            20.4.7 to obtain a consumer credit report about the Client; and/or
          </p>
          <p className="clause">
            20.4.8 allow the credit reporting agency to create or maintain a credit information file containing information about the Client.
          </p>
          <p></p>

          <h3 className="major">Unpaid GPS’S Rights To<br />Dispose Of Goods</h3>
          <p className="clause">
            21.1 In the event that: –
          </p>
          <p className="clause">
            21.1.1 GPS retains or regains possession or control of the Goods; and
          </p>
          <p className="clause">
            21.1.2 Payment of the Price is due to GPS; and
          </p>
          <p className="clause">
            21.1.3 GPS has made demand in writing of the Client for payment of the Price in terms of this contract; and
          </p>
          <p className="clause">
            21.1.4 GPS has not received the Price of the Goods, then, whether the property in the Goods has passed to the Client or has remained with GPS, GPS may dispose of the Goods and may claim from the Client the loss to GPS on such disposal.
          </p>
          <p></p>

          <h3 className="major">Building And<br />Construction Industry<br />Security Of Payments Act</h3>
          <p className="clause">
            (state specific)
          </p>
          <p className="clause">
            22.1 At GPS’s sole discretion, if there are any disputes or claims for unpaid Goods and/or Services then the provisions of the Building and Construction Industry Security of Payments Act (state specific) may apply.
          </p>
          <p className="clause">
            22.2 Nothing in this agreement is intended to have the effect of contracting out of any applicable provisions of the Building and Construction Industry Security of Payments Act (state specific), except to the extent permitted by the Act where applicable.
          </p>
          <p></p>

          <h3 className="major">Lien & Stoppage In Transit</h3>
          <p className="clause">
            23.1 Where GPS has not received or been tendered the whole of the Price, or the payment has been dishonoured, the GPS shall have: –
          </p>
          <p className="clause">
            23.1.1 a lien on the goods;
          </p>
          <p className="clause">
            23.1.2 the right to retain them for the Price while GPS is in possession of them:
          </p>
          <p className="clause">
            23.1.3 a right of stopping the goods in transit whether or not delivery has been made or ownership has passed, and
          </p>
          <p className="clause">
            23.1.4 a right of resale,
          </p>
          <p className="clause">
            23.1.5 the foregoing right of disposal, provided that the lien of GPS shall continue despite the commencement of proceedings or judgement for the Price having been obtained.
          </p>
          <p></p>

          <h3 className="major">Excavation</h3>
          <p className="clause">
            24.1 Unless otherwise stated in writing by GPS, the cost or quoted price in respect of all digging, excavation, dewatering and similar works (Excavation Works) is based on the soil or ground being clean and not dense (e.g. clay soil) or rocky (Unfavourable Soil).
          </p>
          <p className="clause">
            24.2 If GPS encounters Unfavourable Soil whilst undertaking the Services, then GPS may:
          </p>
          <p className="clause">
            24.2.1 delay or extend the date that Services are due to be completed;
          </p>
          <p className="clause">
            24.2.2 charge an additional fee or cost to cover the cost in respect of the additional work and time required to undertake the Services (including the Excavation Works).
          </p>
          <p></p>

          <h3 className="major">Excavation And Concrete Works</h3>
          <p className="clause">
            25.1 Unless otherwise stated in writing by GPS, the cost to undertake:
          </p>
          <p className="clause">
            25.1.1 Excavation Works;
          </p>
          <p className="clause">
            25.1.2 Concrete cutting or drilling or similar works (Concrete Works); or
          </p>
          <p className="clause">
            25.1.3 The reinstatement of driveway, footpath, court paving, gardens, lawn and similar areas will be in addition to the price quoted in any Job Order.
          </p>
          <p className="clause">
            25.4 Any quoted price in respect of Concrete Works is based on a concrete thickness of 150mm. If GPS encounters a concrete thickness of greater than 150mm whilst undertaking Concrete Works then GPS may (in its sole discretion) charge an additional fee or cost to cover the cost in respect of the additional work, equipment or time required to undertake such Concrete Works.
          </p>
          <p></p>

          <h3 className="major">Buried Or Unseen Services</h3>
          <p className="clause">
            27.1 The Client will indemnify GPS and keep GPS indemnified against any liability, loss, claim or proceedings of any kind (whether arising under statute or common law) arising from services which are buried, or unseen being disturbed or damaged. GPS will not be liable for any repair work and any repair work required will be paid at the Clients expense.
          </p>
          <p className="clause">
            Such liability, loss, claims or proceedings includes but is not limited to: –
          </p>
          <p className="clause">
            27.2.1 Damage to the property, real or personal;
          </p>
          <p className="clause">
            27.2.2 Death or personal injury; and
          </p>
          <p className="clause">
            27.2.3 Consequential or economic loss of any kind.
          </p>
          <p></p>

          <h3 className="major">Drain And Sewer</h3>
          <p className="clause">
            26.1 The Customer acknowledges that:
          </p>
          <p className="clause">
            26.1.1 The presence of plant or tree root growth within pipes or drains or blocked, broken or restricted pipes or drains (Damaged Pipes) may indicate that such pipes or drains are damaged;
          </p>
          <p className="clause">
            26.1.2 Damaged Pipes cannot be permanently fixed by cleaning the Damaged Pipes or removing or dislodging plant or tree root growth or such other items which are causing restriction or blockage within Damaged Pipes;
          </p>
          <p className="clause">
            26.1.3 The Customer is liable to pay GPS in the event that GPS (acting reasonably) is unable to unblock Damaged Pipes;
          </p>
          <p className="clause">
            26.1.4 GPS provides no warranty in respect of Services undertaken to unblock or clean Damaged Pipes, including (but not limited to), future or additional blockages or restrictions located within Damaged Pipes that GPS has attempted to unblock or clean.
          </p>
          <p className="clause">
            26.2 The Customer further acknowledges that:
          </p>
          <p className="clause">
            26.2.1 Close circuit television equipment, drainage cameras and other pipe inspection equipment (Specialised Pipe Equipment) may be used by GPS in its sole discretion in an attempt to locate the cause of Damaged Pipes;
          </p>
          <p className="clause">
            26.2.2 GPS may charge an additional fee or cost if Specialised Pipe Equipment is used by GPS
          </p>
          <p></p>

          {close}
        </article>
      </div>
    )
  }
}

Main.propTypes = {
  route: PropTypes.object,
  article: PropTypes.string,
  articleTimeout: PropTypes.bool,
  onCloseArticle: PropTypes.func,
  timeout: PropTypes.bool,
  setWrapperRef: PropTypes.func.isRequired,
}

export default Main
