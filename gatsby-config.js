module.exports = {
  siteMetadata: {
    title: `Greeny's Plumbing Solutions`,
    author: `Matthew Baldwin`,
    description: `All Cisterns Go!`,
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Greeny's Plumbing Solutions`,
        short_name: 'Greeny',
        start_url: '/',
        background_color: '#1a1a1a',
        theme_color: '#1a1a1a',
        display: 'minimal-ui',
        icon: 'src/images/logo.svg', // This path is relative to the root of the site.
      },
    },
    'gatsby-plugin-sass',
    'gatsby-plugin-offline'
  ],
}
